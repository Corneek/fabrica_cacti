<?php
	include("head.php");
	include("menu.php");
?>



<style>
	img:hover {
		box-shadow: 0 0 2px 1px rgba(31,58,5, 0.5);
	}
	
	.content-left {
	width: 20%;
	float: left;
	margin-left: 10%;
	}

	.content-center {
	width: 20%;
	float: left;
	margin-left: 10%;
	}

	.content-right {
	width: 20%;
	float: right;
	margin-right: 10%;
	}
</style>

	<div style="overflow:hidden;">
		
	<div style="float: left; width: 22%; background-color:#1F3A05; padding-top: 2vw; padding-left: 2vw; padding-right: 2vw; padding-bottom: 99999px; margin-bottom: -99999px; color: white;">
		
		<form class="form-inline my-2 my-lg-0">
			<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" style="width: 14vw; margin-bottom: 1vw">
			<button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
		</form>
		<div>
		<br>
		<h6>Kategorie</h6>
		<ul style="list-style-type: none; font-size: 0.8rem">
			<li><a href="products.php?action=otherLeather">Skóra-inne</a></li>
			<li><a href="products.php?action=shoes">Buty</a></li>
			<li><a href="products.php?action=fabrics">Tekstylia</a></li>
			<li><a href="products.php?action=armory">Uzbrojenie</a></li>
			<li><a href="products.php?action=wood">Drewno</a></li>
			<li><a href="products.php?action=other">Inne</a></li>
			<li><a href="products.php?action=all">Wszystko</a></li>
		</ul>
		</div>
	</div>
<?php	
	if (isset($_GET['action']) && $_GET['action'] == "otherLeather") {

		$stringOtherLeather = "SELECT * FROM products p LEFT JOIN products_images pi ON p.id = pi.products_id WHERE id_categories LIKE '1'";

		printProducts($db, $stringOtherLeather);

	} elseif (isset($_GET['action']) && $_GET['action'] == "shoes") {

		$stringShoes = "SELECT * FROM products p LEFT JOIN products_images pi ON p.id = pi.products_id WHERE id_categories LIKE '2'";

		printProducts($db, $stringShoes);

	} elseif (isset($_GET['action']) && $_GET['action'] == "fabrics") {

		$stringFabrics = "SELECT * FROM products p LEFT JOIN products_images pi ON p.id = pi.products_id WHERE id_categories LIKE '3'";

		printProducts($db, $stringFabrics);

	} elseif (isset($_GET['action']) && $_GET['action'] == "armory") {

		$stringArmory = "SELECT * FROM products p LEFT JOIN products_images pi ON p.id = pi.products_id WHERE id_categories LIKE '4'";

		printProducts($db, $stringArmory);

	} elseif (isset($_GET['action']) && $_GET['action'] == "wood") {

		$stringWood = "SELECT * FROM products p LEFT JOIN products_images pi ON p.id = pi.products_id WHERE id_categories LIKE '5'";

		printProducts($db, $stringWood);

	} elseif (isset($_GET['action']) && $_GET['action'] == "other") {

		$stringOther = "SELECT * FROM products p LEFT JOIN products_images pi ON p.id = pi.products_id WHERE id_categories LIKE '6'";

		printProducts($db, $stringOther);

	} elseif (isset($_GET['action']) && $_GET['action'] == "all" || !isset($_GET['action'])) {

		$stringAll = "SELECT * FROM products p LEFT JOIN products_images pi ON p.id = pi.products_id ORDER BY id_categories ASC";

		printProducts($db, $stringAll);

	} elseif (isset($_GET['action']) && $_GET['action'] == "productId" ) {
		echo '<div style="margin-left:auto; margin-right:auto; width: 75%; float:left; border: 2px solid black; padding: 2vw 2vw; padding-bottom: 99999px; margin-bottom: -99999px;">
		<h2>fsa<br>fas<br>dfsssssssssssssssssssssssssssssssssss</h2>
		</div>';
	} 

	echo '</div>';
// --------------------------------------------------------------------------------------------------------------------------

		function printColumn ($columnNumber, $table) 
		{
		 
			$numRows = mysqli_num_rows($table);	

			for ($i=1; $i <=$numRows ; $i++) 
			{ 
				$products_data = $table -> fetch_object();
				$id = $products_data -> id;
				$name = $products_data -> name;
				$type = $products_data -> type;
				$imagePath = $products_data -> image_path;
				
				if ($columnNumber == $i) 
				{
					$row = str_replace("/", "\\", $imagePath);

					echo '
					<div style="height: 22vw"><a href="products.php?action=productId='.$id.'"><img src='.$row.' style="border-radius: 8px; max-width: 100%; height: auto; margin-bottom: 1vw; border:1px solid #ddd; padding: 5px"></a></div>';
					$columnNumber +=3;	
					
				}
			}
		}

	function printProducts ($db, $stringQuery)
	{
		echo'
			<div style="margin-left:auto; margin-right:auto; width: 75%; float:left; border: 2px solid black; padding: 2vw 2vw; padding-bottom: 99999px; margin-bottom: -99999px;">

			<h3 style="text-align:center;"><b>Nasze produkty</b></h3>';

		if (mysqli_num_rows($db -> query($stringQuery)) > 0) 
		{

			echo '<div class="content-left" style="border: 2px solid red;">';
				$products = $db -> query($stringQuery);
				printColumn(1, $products);
					
			echo '</div>
				<div class="content-center" style="border: 2px solid blue;">';
				$products = $db -> query($stringQuery);
				printColumn(2, $products);

			echo '</div>
				<div class="content-right" style="border: 2px solid yellow;">';
				$products = $db -> query($stringQuery);
				printColumn(3, $products);

			echo '</div></div>';
		} else {
			echo "Brak produktów w danej kategorii";
		}
	}

?>


<?php include("foot.php");?>